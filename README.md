# Projet_4



## Getting started

 [Lien vers kaggle](https://www.kaggle.com/datasets/olistbr/brazilian-ecommerce?resource=download)
 [Lien vers OCR](https://openclassrooms.com/fr/paths/148/projects/630/assignment)



Milestone 1 : Analyse exploratoire
Livrable :
Notebook, partie préparation du fichier des commandes et de son analyse exploratoire.
Niveau d’avancement : 10 %
Problèmes et erreurs courants :
L‘identifiant unique d’un client est le « customer_unique_id », à utiliser pour regrouper les commandes par client (un « customer_id » différent est associé à chaque commande).
Recommandations :
Analyser le contenu de chaque table mise à disposition (features, valeurs).
Préparer un fichier de commandes, par « merge » des différentes tables.
Réaliser un describe(), vérifier s’il y a des valeurs manquantes.
Analyser par exemple la distribution du nombre de commandes par client, la distribution des montants, la distribution des catégories des produits etc.
L’étudiant constatera que seuls 3 % des clients ont réalisé plus d’une commande.
Milestone 2 : Création d’un fichier par client
Livrable :
Notebook, partie feature engineering de création de features par client, à partir du fichier des commandes.
Niveau d’avancement : 20 %
Problèmes et erreurs courants :
Erreur consistant à supprimer les 3 % de clients qui ont plus d’une commande : ce sont les meilleurs clients, donc à conserver d’un point de vue métier marketing afin de les cibler et les gérer.
Certaines données sont liées à la commande (exemple : « payment_value » montant de la commande, ou « review_score ») ; d’autres sont liées à la ligne de commande (exemple : « price »), donc attention lors de la réalisation d’agrégations, en fonction du fichier utilisé (par commande « order_id », ou ligne de commande « order_item_id »).
Erreur consistant à prendre trop de features au départ et de natures très différentes, qui conduiraient à des clusters qui n’auraient aucun sens métier.
Recommandations :
Conserver toutes les commandes, notamment celles des 3 % de clients qui en ont fait plusieurs.
Se concentrer dans un premier temps sur quelques features qui ont du sens du point de vue marketing, pour cibler les clients plus ou moins intéressants en termes de vente : RFM (Récence : durée depuis la dernière commande, Fréquence : nombre de commandes, Montant: par exemple montant cumulé des commandes).
Attention sur RFM : il faut garder des valeurs continues des features R, F, M, et non pas calculer des quantiles qui créeraient une distorsion de données (comme ce qui était réalisé autrefois sans machine learning).
Réaliser l’étape suivante de clustering avec ces 3 features, puis faire d’autres simulations avec des features supplémentaires, par exemple « review_score », voire d’autres features qui pourraient apporter de la valeur pour séparer les bons clients des autres.
Attention : l’ajout de features catégorielles comme la catégorie peut provoquer un brouillage du clustering, qui n’aurait plus de sens d’un point de vue métier. Cela peut faire partie de tests de la part de l’étudiant, mais n’est pas obligatoire.
Idéalement, la création du fichier par client sera développée via un « df.groupby(…).agg(…) ».
Ressources :
Méthode RFM : https://www.definitions-marketing.com/definition/segmentation-rfm/